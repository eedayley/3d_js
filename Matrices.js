
class Matrix {
	subtract(matrix_2) {
		var matrix_3 = [];
		var t = [];
		for (let i = 0; i < this.value.length; i++) {
			t = [];
			for (let j = 0; j < this.value[i].length; j++) {
				t.push(this.value[i][j]-matrix_2.value[i][j]);
			}
			matrix_3.push(t);
		}
	return matrix_3;
	}

	constructor(matrix_list) {
		this.value = matrix_list;
		this.columns = this.value.length;
		this.rows = this.value[0].length;
	}
};

"use strict";

function drawLine(x1, y1, x2, y2, strokeColor, fillColor) {
	ctx.strokeStyle = strokeColor;
	ctx.filleStyle = fillColor;
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.stroke();
}

function getNormCoords(xylist) {
	return [xylist[0]+(canWidth/2), xylist[1]+(canHeight/2)];
}

function get2D(a, c) {
	var ax = a[0];
	var ay = a[1];
	var az = a[2];
	var cx = c[0];
	var cy = c[1];
	var cz = c[2];
	var am = new Matrix([[ax, ay, az]]);
	var cm = new Matrix([[cx, cy, cz]]);
	
	var dm = new Matrix(am.subtract(cm));
	var dx  = dm.value[0][0];
	var dy  = dm.value[0][1];
	var dz  = dm.value[0][2];
	try {
		var bx = ((cz/dz)*dx)-dx;
		var by = ((cz/dz)*dy)-dy;
		return [bx, by];
	}
	catch (err) {
		return [ax, ay];
	}

}


function getDistance(a, b) {
	let s = 0;
	for (let i = 0; i < a.length; i++) {
		s += ((a[i]-b[i])*(a[i]-b[i]));
	}
	return Math.sqrt(s);
}

function getPix(a, b) {
	var l = [];
	var d = dist(a, b);
	for (let i = 0; i < Math.trunc(d); i++) { 
		l.push([a[0]+i*((b[0]-a[0])/d), a[1]+i*((b[1]-a[1])/d), a[2]+i*((b[2]-a[2])/d)])
	}
	l.push([b[0], b[1], b[2]])
	return l
}

function drawSquare(center, factor, z, fill="green", outline="red") {
	var c = center;
	var verts3d = [[c[0]-factor, c[1]-factor],
	          [c[0]+factor, c[1]-factor],
	          [c[0]+factor, c[1]+factor],
	          [c[0]-factor, c[1]+factor],
	          [c[0]-factor, c[1]-factor],];

	var verts2d = [];
	var temp = [];
	for (let i = 0; i < verts3d.length; i++) {
		temp = verts3d[i];
		temp.push(z);
		verts2d.push(getNormCoords(get2D(temp, camera)));
	}
	ctx.beginPath();
	ctx.strokeStyle = outline;
	ctx.fillStyle = fill;
	ctx.moveTo(verts2d[0], verts2d[1]);
	for (let i = 0; i < verts2d.length; i++) {
		ctx.lineTo(verts2d[i][0], verts2d[i][1]);
	}
	ctx.stroke();
	ctx.fill();
	ctx.closePath();

}

function drawPoly(verts2d) {
	ctx.beginPath();
	ctx.strokeStyle = 'white';
	ctx.fillStyle = 'green';
	ctx.moveTo(verts2d[0], verts2d[1]);
	for (let i = 0; i < verts2d.length; i++) {
		ctx.lineTo(verts2d[i][0], verts2d[i][1]);
	}
	ctx.stroke();
	ctx.fill();
	ctx.closePath();

}

function drawShape() {
	var verts3d = [[-200, -200, -500],
		       [-200, 200,  -500],
		       [200,  200, -10000]];
	var verts2d = [];
	var temp = [];
	for (let i = 0; i < verts3d.length; i++) {
		temp = verts3d[i];
		verts2d.push(getNormCoords(get2D(temp, camera)));
	}
	ctx.beginPath();
	ctx.strokeStyle = 'blue';
	ctx.fillStyle = 'red';
	ctx.moveTo(verts2d[0], verts2d[1]);
	for (let i = 0; i < verts2d.length; i++) {
		ctx.lineTo(verts2d[i][0], verts2d[i][1]);
	}
	ctx.stroke();
	ctx.fill();
	ctx.closePath();
}

function drawCube(center, x, y, z, fill='green', outline='black') {
	var distx = x/2;
	var disty = y/2;
	var distz = z/2;
	var x1 = center[0]+distx;
	var x2 = center[0]-distx;
	var y1 = center[1]+disty;
	var y2 = center[1]-disty;
	var z1 = center[2]+distz;
	var z2 = center[2]-distz;

	var c1 = [x1, y1, z1];
	var c2 = [x2, y1, z1];
	var c3 = [x1, y2, z1];
	var c4 = [x2, y2, z1];

	var c5 = [x1, y1, z2];
	var c6 = [x2, y1, z2];
	var c7 = [x2, y2, z2] ;
	var c8 = [x1, y2, z2] ;

	var f1 = [c1, c2, c4, c3, c1];
	var f2 = [c1, c5, c6, c2, c1];
	var f3 = [c2, c6, c7, c4, c2];
	var f4 = [c4, c3, c8, c7, c4];
	var f5 = [c3, c1, c5, c8, c3];

	var f6 = [c5, c6, c7, c8, c5];

	var cube = [f6, f5, f4, f3, f2, f1];

	let tmp = [];

	for (let i = 0; i < cube.length; i++) {
		tmp = [];
		for (let j = 0; j < cube[i].length; j++) {
			tmp.push(getNormCoords(get2D(cube[i][j], camera)));
		}
		drawPoly(tmp);
	
	}
	return cube;

}

function initCanvas() {
	canvas = document.getElementById("mainCanvas");
	winWidth = $(window).width();
	winHeight = $(window).height();
	canWidth = winWidth;
	canHeight = winHeight;
	canvas.width = winWidth;
	canvas.height = winHeight;
	ctx = canvas.getContext("2d");
}
function clear() {
	ctx.clearRect(0,0,canvas.width, canvas.height);
}
function refresh() {
	clear();
	//drawSquare([0, 0], 100, -100000);
	//drawShape();
	c = drawCube([0,0,-1000], 200, 200, 200);
}

function keyUpRouter(e) {
	if (e.which == 16) {
		shiftKey = 0;
	}
}

function keyDownRouter(e) {
	console.log(e.which);
	var moveBy = 5;
	var zMoveBy = 1000;
	if (e.which == 16) {
		shiftKey = 1;
	}
	else if (e.which == 39) {
		//RIGHT ARROW
		camera[0] = camera[0] - moveBy;
	}
	else if (e.which == 37) {
		//LEFT ARROW
		camera[0] = camera[0] + moveBy;
	}
	else if (e.which == 38) {
		//UP ARROW
		camera[1] = camera[1] + moveBy;
	}
	else if (e.which == 40) {
		//DOWN ARROW
		camera[1] = camera[1] - moveBy;
	}
	else if (e.which == 189) {
		//hyphen or minus sign
		camera[2] = camera[2] - zMoveBy;
	}
	else if (shiftKey) {
		if (e.which == 187) {
			//Plus sign
			camera[2] = camera[2] + zMoveBy;
		}
		else {
			return;
		}
	}
	else {
		return;
	}
	e.preventDefault();
	refresh();
}

//Initialize globals
var camera = [0, 0, 2000];
var origin = camera.slice(0, 2);
var canvas;
var ctx;
var winWidth;
var winHeight;
var canWidth;
var canHeight;
var shiftKey; //Whether the shift key is currently pressed

var c;

$(document).ready(
		function() {
			$(document).on('keydown', keyDownRouter);
			$(document).on('keyup', keyUpRouter);
			initCanvas();
			refresh();
		});
//NOTES:
//every face is a triangle
//each pixel within that triangle must be rendered seperately in order to keep the canvas non-cubist
//


